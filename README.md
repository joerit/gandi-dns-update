a golang app that updates a gandi record


# usage
1) set ENV vars
2) go build; go run .

uses the following ENV vars:

DOMAIN                  domain the record lives in  e.g. example.com

RECORD                  record we're updating       e.g. www

GANDI_ACCESS_TOKEN      gandi PAT to use
