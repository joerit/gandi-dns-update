package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
)

func main() {
	var domain = os.Getenv("DOMAIN")
	fail_if_equal(domain, "", "missing env var: DOMAIN")
	var record = os.Getenv("RECORD")
	fail_if_equal(record, "", "missing env var: RECORD")

	var gandi_access_token = os.Getenv("GANDI_ACCESS_TOKEN")
	fail_if_equal(gandi_access_token, "", "missing env var: GANDI_ACCESS_TOKEN")

	var new_ip = get_external_ip()
	fmt.Printf("new ip is %s\n", new_ip)
	var old_ip = get_dns_ip(domain, record, gandi_access_token)
	fmt.Printf("current ip is %s\n", old_ip)
	if new_ip == old_ip {
		fmt.Printf("ip is still up to date, exiting\n")
	} else {
		fmt.Printf("pushing ip update\n")
		update_ip(new_ip, domain, record, gandi_access_token)
	}
	os.Exit(0)
}

func fail_if_equal(a any, b any, templates ...string) {
	if a == b {
		for _, template := range templates {
			fmt.Printf(template, a, b)
		}
		os.Exit(1)
	}
}
func fail_if_not_equal(a any, b any, templates ...string) {
	if a != b {
		for _, template := range templates {
			fmt.Printf(template, a, b)
		}
		os.Exit(1)
	}
}

func use(a any) {
	_ = a
}

type Ipify_Response struct {
	Ip string `json:"ip"`
}

func get_external_ip() string {
	var resp, err = http.Get("https://api.ipify.org?format=json")
	fail_if_not_equal(err, nil)
	defer resp.Body.Close()

	var response Ipify_Response
	json.NewDecoder(resp.Body).Decode(&response)
	return response.Ip

}

type Gandi_Domain_Record struct {
	RrsetName   string   `json:"rrset_name"`
	RrsetTTL    int      `json:"rrset_ttl"`
	RrsetType   string   `json:"rrset_type"`
	RrsetValues []string `json:"rrset_values"`
	RrsetHref   string   `json:"rrset_href"`
}

func get_dns_ip(domain string, record string, gandi_access_token string) string {
	var req, req_err = http.NewRequest(
		"GET",
		fmt.Sprintf("https://api.gandi.net/v5/livedns/domains/%s/records/%s/A", domain, record),
		nil,
	)
	fail_if_not_equal(req_err, nil)
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", gandi_access_token))

	var client = http.Client{}
	var resp, err = client.Do(req)
	fail_if_not_equal(err, nil)
	defer resp.Body.Close()

	var response Gandi_Domain_Record
	json.NewDecoder(resp.Body).Decode(&response)
	return response.RrsetValues[0]
}
